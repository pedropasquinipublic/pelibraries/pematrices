from torch import tensor, stack, cos, sin, exp, arctan2, arcsin, eye, zeros, cfloat, Tensor

def peUnitary3D(**parameters: dict[str, Tensor]) -> Tensor:
    r"""
    Calculates a 3x3 unitary matrix based on the given angles and phases lists.

    This function receives a dictionary containing parameters representing angles and phases, 
    and computes the corresponding unitary matrix using the parametrization of the Particle Data Group (PDG) 2024 
    for the neutrino mixing matrix.

    Parameters:
        parameters (dict[str, Tensor]): A dictionary containing the following keys representing angles and phases:

            - 'th12': Angle representing the mixing between the first and second neutrino generations.
            - 'th13': Angle representing the mixing between the first and third neutrino generations.
            - 'th23': Angle representing the mixing between the second and third neutrino generations.
            - 'dCP': Dirac CP Phase parameter.
            - 'eta1': Majorana phase parameter.
            - 'eta2': Majorana phase parameter.

    Returns:
        Tensor: A 3x3 complex-valued unitary matrix.

    """

    # Extract parameters from the dictionary
    cos_th12 = cos(parameters["th12"])
    sin_th12 = sin(parameters["th12"])

    cos_th13 = cos(parameters["th13"])
    sin_th13 = sin(parameters["th13"])

    cos_th23 = cos(parameters["th23"])
    sin_th23 = sin(parameters["th23"])

    edCP  = exp(1.0j*parameters["dCP"])
    cedCP = exp(-1.0j*parameters["dCP"])

    exp_eta1 = exp(1.0j*parameters["eta1"])    
    exp_eta2 = exp(1.0j*parameters["eta2"])

    # Stack components, transpose, and reshape them to the 3x3 shape
    return stack((
        cos_th12*cos_th13*exp_eta1, 
        cos_th13*sin_th12, 
        sin_th13*cedCP*exp_eta2,

        -exp_eta1*(cos_th23*sin_th12 + edCP*cos_th12*sin_th13*sin_th23), 
        cos_th12*cos_th23 - edCP*sin_th12*sin_th13*sin_th23, 
        exp_eta2*cos_th13*sin_th23, 

        exp_eta1*(sin_th12*sin_th23 - edCP*cos_th23*cos_th12*sin_th13), 
        - edCP*cos_th23*sin_th12*sin_th13 - cos_th12*sin_th23, 
        exp_eta2*cos_th13*cos_th23
    )).transpose(-1,-2).reshape((*cos_th12.shape,3, 3))

def peUPMNS(**parameters: dict[str, Tensor]) -> Tensor:
    r"""
    Calculates a 3x3 unitary matrix based on the given angles and phases lists.

    This function receives a dictionary containing parameters representing angles and phases, 
    and computes the corresponding unitary matrix using the parametrization of the Particle Data Group (PDG) 2024 
    for the neutrino mixing matrix without including the Majorana phases 
    (this function should be slightly faster/less memory consuming than peUnitary3D)

    Parameters:
        parameters (dict[str, Tensor]): A dictionary containing the following keys representing angles and phases:

            - 'th12': Angle representing the mixing between the first and second neutrino generations.
            - 'th13': Angle representing the mixing between the first and third neutrino generations.
            - 'th23': Angle representing the mixing between the second and third neutrino generations.
            - 'dCP': Dirac CP Phase parameter.

    Returns:
        Tensor: list of 3x3 complex-valued unitary matrix.

    """

    # Extract parameters from the dictionary
    cos_th12 = cos(parameters["th12"])
    sin_th12 = sin(parameters["th12"])

    cos_th13 = cos(parameters["th13"])
    sin_th13 = sin(parameters["th13"])

    cos_th23 = cos(parameters["th23"])
    sin_th23 = sin(parameters["th23"])

    edCP  = exp(1.0j*parameters["dCP"])
    cedCP = exp(-1.0j*parameters["dCP"])

    # Stack components, transpose, and reshape them to the 3x3 shape
    return stack((
        cos_th12*cos_th13, 
        cos_th13*sin_th12, 
        sin_th13*cedCP,

        -(cos_th23*sin_th12 + edCP*cos_th12*sin_th13*sin_th23), 
        cos_th12*cos_th23 - edCP*sin_th12*sin_th13*sin_th23, 
        cos_th13*sin_th23, 

        (sin_th12*sin_th23 - edCP*cos_th23*cos_th12*sin_th13), 
        - edCP*cos_th23*sin_th12*sin_th13 - cos_th12*sin_th23, 
        cos_th13*cos_th23
    )).transpose(-1,-2).reshape((*cos_th12.shape,3, 3))

def WijRotMatrix(i:int, j:int, dim:int, theta: Tensor, delta: Tensor) -> Tensor:
    r"""
    Calculates a rotation matrix over the axis ij based on a list of rotation angles and phases list.

    It creates an identity matrix, modifies it by setting the diagonal elements corresponding to indices i and j to 0,
    and applies rotations based on the given angles and phases.

    Parameters:
        i (int): Index of the first axis (1-based) for rotation.
        j (int): Index of the second axis (1-based) for rotation.
        dim (int): Dimension of the matrix.
        theta (Tensor): List of rotation angles.
        delta (Tensor): List of phase angles.

    Returns:
        Tensor: A list of rotation matrix over the axis ij.

    """

    shape = (1,)*len(theta.shape)

    auxReturn = eye(dim, dtype = cfloat)
    auxReturn[i - 1, i - 1] = 0.0
    auxReturn[j - 1, j - 1] = 0.0

    auxReturn = auxReturn.view(*shape, dim, dim)

    auxReturn =  auxReturn + (eye(dim, dtype = cfloat).view(*shape, dim, dim) - auxReturn)*cos(theta.view(*theta.shape, 1, 1))
    
    _OffDiagij = zeros(dim, dim)
    _OffDiagji = zeros(dim, dim)
    _OffDiagij[i - 1, j - 1] = 1.0
    _OffDiagji[j - 1, i - 1] = -1.0

    return (  auxReturn \
            + _OffDiagij.view(*shape, dim, dim)*sin(theta.view(*theta.shape, 1, 1))*exp(-1.0j*delta.view(*delta.shape, 1, 1))\
            + _OffDiagji.view(*shape, dim, dim)*sin(theta.view(*theta.shape, 1, 1))*exp( 1.0j*delta.view(*delta.shape, 1, 1))
           ).detach()

def peGetMixingAngles(inUunitary: Tensor) -> tuple[Tensor, Tensor, Tensor, Tensor, Tensor, Tensor]:
    r"""
    Extracts mixing angles and phases from a list of unitary matrices.

    This function receives a list of unitary matrices and returns a tuple containing the mixing angles
    and phases corresponding to the parametrization of the Particle Data Group (PDG) 2024.

    Parameters:
        inUunitary (Tensor): List of unitary matrices.

    Returns:
        Tuple[Tensor, Tensor, Tensor, Tensor, Tensor, Tensor]: A tuple containing the following elements:
        
            - $\theta_{12}$: Mixing angle between the first and second neutrino generations.
            - $\theta_{13}$: Mixing angle between the first and third neutrino generations.
            - $\theta_{23}$: Mixing angle between the second and third neutrino generations.
            - $\delta_{\rm CP}$: CP-violating phase.
            - $\eta_1$: Additional Majorana phase.
            - $\eta_2$: Additional Majorana phase.
    """

    # Calculate mixing angles
    th12 = arctan2(inUunitary[..., 0, 1].abs(), inUunitary[..., 0, 0].abs())
    th13 =  arcsin(inUunitary[..., 0, 2].abs())
    th23 = arctan2(inUunitary[..., 1, 2].abs(), inUunitary[..., 2, 2].abs())

    # Calculate Dirac phase
    e_iphi = exp(1.0j*(inUunitary[..., 1, 1].angle() - inUunitary[..., 0, 1].angle()\
            + inUunitary[..., 0, 2].angle() - inUunitary[..., 1, 2].angle()))

    dCP  = ((cos(th12)*cos(th23))/(e_iphi*inUunitary[..., 1, 1].abs() + sin(th12)*sin(th13)*sin(th23))).angle()

    # Calculate additional Majorana phase parameters
    eta1 = inUunitary[..., 0, 0].angle()

    eta2 = inUunitary[..., 0, 2].angle() + dCP
    
    return th12, th13, th23, dCP, eta1, eta2

if __name__ == "__main__":
    
    from torch import double, pi

    MyUpmns = peUnitary3D(
                th12 = pi*tensor([25.0,  33.66], dtype=double)/180.0, 
                th13 = pi*tensor([20.0,   8.54], dtype=double)/180.0, 
                th23 = pi*tensor([15.0,  49.10], dtype=double)/180.0, 
                dCP  = pi*tensor([45.0, 197.00], dtype=double)/180.0, 

                eta1 = tensor([2.0, 0.0], dtype=double), 
                eta2 = tensor([5.0, 0.0], dtype=double)
                
                )


    print("The matrices from the angles are:")
    print(MyUpmns)

    print("The angles we used to construct the matrix are:")

    th12, th13, th23, dCP, eta1, eta2 = peGetMixingAngles(MyUpmns)
    print("\tth_12: ", 180.0*th12/pi, "degrees")
    print("\tth_13: ", 180.0*th13/pi, "degrees")
    print("\tth_23: ", 180.0*th23/pi, "degrees")
    print("\td_CP: ", 180.0*dCP/pi, "degrees")
    print("\teta1: ", eta1, "rad")
    print("\teta2: ", eta2, "rad")

