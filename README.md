_______________________________________________
<div align="center">
peMatrices

A Python library for performing matrix operations with PyTorch in a broadcastable way.

Creation date: 09 May 2024          
Created by 

Pedro Pasquini      
<https:/inspirehep.net/authors/1467863>    
</div>

_______________________________________________

# Introduction
The peMatrices is a Python library for performing matrix operations with PyTorch in a broadcastable way.
For the unitary matrices the code assumes the PDG [1] neutrino mixing angle parametrization.

# Instalation

To **install** you can use pip. 

Go to folder in terminal and run 
>> pip3 install -e ../pematrices

To **uninstall** it just type
>> pip3 uninstall pematrices

# Usage
See example folder for examples of usage.

# References

[1] R. L. Workman <em>et al.</em> [Particle Data Group],,
''<em>Review of Particle Physics</em>'',
<a href="http://dx.doi.org/10.1093/ptep/ptac097">
PTEP 2022, 083C01 (2022)</a>.
