from setuptools import setup, find_packages

setup(
    name='pematrices',
    version='1.0.0',    
    description='A Python library for performing matrix operations with PyTorch in a broadcastable way.',
    url='https://gitlab.com/pedropasquinipublic/pelibraries/pe-matrix-ops',
    author='Pedro Pasquini',
    author_email='pedrosimpas@gmail.com',
    long_description_content_type="text/markdown",
    packages=find_packages(),
    install_requires=[ 
                      'torch'              
                      ],
    classifiers=[
        'Intended Audience :: Science/Research', 
        'Operating System :: POSIX :: Linux',      
        'Programming Language :: Python :: 3.8',
    ],
)
