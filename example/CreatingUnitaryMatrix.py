##################################################
##################################################
##                                              ##
##                                              ##
##                 created by                   ##
##               Pedro Pasquini                 ##
##            Created: 09 May 2024              ##
##    https://inspirehep.net/authors/1467863    ##
##                                              ##
##                                              ##
##################################################
##################################################

####################################################
####################################################
##   This code has an example on how to use       ##
##            pematrices library                  ##
####################################################
####################################################

from torch import double, pi, tensor
from pematrices import peUnitary3D, peGetMixingAngles, MatMultiply, ConjugateTranspose

if __name__ == "__main__":

    Vunitary = peUnitary3D(
                th12 = pi*tensor([25.0,  33.66], dtype=double)/180.0, 
                th13 = pi*tensor([20.0,   8.54], dtype=double)/180.0, 
                th23 = pi*tensor([15.0,  49.10], dtype=double)/180.0, 
                dCP  = pi*tensor([45.0, 197.00], dtype=double)/180.0, 

                eta1 = tensor([2.0, 0.0], dtype=double), 
                eta2 = tensor([5.0, 0.0], dtype=double)
                
                )


    print("The matrices from the angles are:")
    print(Vunitary)
    print()

    print("Checking if matrices are indeed unitary")
    print(MatMultiply(Vunitary, ConjugateTranspose(Vunitary)))
    print()

    print("The angles we used to construct the matrix are:")

    th12, th13, th23, dCP, eta1, eta2 = peGetMixingAngles(Vunitary)
    print("\tth_12: ", 180.0*th12/pi, "degrees")
    print("\tth_13: ", 180.0*th13/pi, "degrees")
    print("\tth_23: ", 180.0*th23/pi, "degrees")
    print("\td_CP: ", 180.0*dCP/pi, "degrees")
    print("\teta1: ", eta1, "rad")
    print("\teta2: ", eta2, "rad")
