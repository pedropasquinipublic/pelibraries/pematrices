"""
       -- pe_matrix_ops -- 

A Python library for performing matrix operations with PyTorch in a broadcastable way.
"""

# creation date 09 May 2024 
__version__ = "1.0.0"
__author__ = 'Pedro Pasquini'
__credits__ = 'https://inspirehep.net/authors/1467863'

from pematrices.peMatricesOperations import *
from pematrices.peUnitaryMatrix import *
from pematrices.peMatrixCreation import *