from torch import  stack, zeros, Tensor

def peDiagonal3x3Matrix(d1: Tensor, d2: Tensor, d3:Tensor) -> Tensor:
    """
    Constructs a 3x3 diagonal matrix from given diagonal elements.

    This function constructs a 3x3 diagonal matrix from the given diagonal elements `d1`, `d2`, and `d3`.
    The off-diagonal elements are set to zero.

    Parameters:
        d1 (Tensor): Diagonal element at position (1, 1).
        d2 (Tensor): Diagonal element at position (2, 2).
        d3 (Tensor): Diagonal element at position (3, 3).

    Returns:
        Tensor: A 3x3 diagonal matrix with the given diagonal elements.
    """

    # Stack components and reshape them to the 3x3 shape
    return stack((
        d1, 
        zeros(d1.shape), 
        zeros(d1.shape), 

        zeros(d1.shape), 
        d2, 
        zeros(d1.shape), 

        zeros(d1.shape), 
        zeros(d1.shape), 
        d3
    )).transpose(-1, -2).reshape((*d1.shape,3, 3))
