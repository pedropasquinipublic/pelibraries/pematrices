from torch import Tensor, einsum

def Transpose(inMat: Tensor) -> Tensor:
    """
    Computes the transpose of a tensor containing matrices.

    This function calculates the transpose of the input tensor `inMat`.
    It transposes the tensor along its last two dimensions.

    Parameters:
        inMat (Tensor): The input tensor.

    Returns:
        Tensor: The transpose of the input tensor.
    """
    return inMat.transpose(-1,-2)

def ConjugateTranspose(inMat: Tensor) -> Tensor:
    """
    Computes the conjugate transpose of a tensor containing matrices.

    This function calculates the conjugate transpose of the input tensor `inMat`.
    It first takes the complex conjugate of all elements in the tensor and then
    transposes the tensor along its last two dimensions.

    Parameters:
        inMat (Tensor): The input tensor.

    Returns:
        Tensor: The conjugate transpose of the input tensor.
    """
    return inMat.conj().transpose(-1,-2)

def Trace(inMat1: Tensor) -> Tensor:
    """
    Computes the trace of a tensor.

    This function calculates the trace of the input tensor `inMat1`, which is the sum of the diagonal elements.
    It uses Einstein summation to sum the diagonal elements along the last two dimensions of the tensor.

    Parameters:
        inMat1 (Tensor): The input tensor.

    Returns:
        Tensor: The trace of the input tensor.
    """
    return einsum("...ii->...", inMat1)

def MatMultiply(inMat1: Tensor, inMat2: Tensor) -> Tensor:
    """
    Performs the broadcastable operation of matrix multiplication.

    This function computes the matrix multiplication of two tensors inMat1 and inMat2.
    It performs the operation in a broadcastable manner, meaning it supports broadcasting 
    over additional dimensions.

    Parameters:
        inMat1 (Tensor): First tensor to be multiplied.
        inMat2 (Tensor): Second tensor to be multiplied.

    Returns:
        Tensor: The result of the matrix multiplication inMat1 . inMat2.

    """
    return einsum("...ik,...kj->...ij", inMat1, inMat2)

